# frozen_string_literal: true

require_relative "lib/jgrind"

DATA_DIR = ENV.fetch("DATA_DIR", "data")
directory DATA_DIR

TMP_DIR = ENV.fetch("TMP_DIR", "tmp")
directory TMP_DIR

task default: :build

desc "Build the project"
task build: %w[clean jmdict:build kanjidic:build]

desc "Remove temporary files"
task :clean do
  rm_rf TMP_DIR
end

JMDICT_XML = File.join(TMP_DIR, "jmdict.xml")
file JMDICT_XML: TMP_DIR do
  Rake::Task["jmdict:download_e"].execute
end

JMDICT_JSONL = File.join(DATA_DIR, "jmdict/jmdict.jsonl")
file JMDICT_XML do
  Rake::Task["jmdict:convert"].execute
end

namespace :jmdict do
  task build: %w[clean download_e convert_priority]

  desc "Download jmdict.xml (Full version)"
  task download: TMP_DIR do
    puts "Downloading jmdict.xml (Full version) ..."
    Jgrind::Jmdict.download_to_file(JMDICT_XML)
    puts "Downloaded jmdict.xml (Full version)."
  end

  desc "Download jmdict.xml (English version)"
  task download_e: TMP_DIR do
    puts "Downloading jmdict.xml (English version) ..."
    Jgrind::Jmdict.download_e_to_file(JMDICT_XML)
    puts "Downloaded jmdict.xml (English version)."
  end

  desc "Convert jmdict.xml to jmdict.jsonl"
  task convert: JMDICT_XML do
    puts "Converting jmdict.xml to jmdict.jsonl ..."
    Jgrind::Jmdict.convert_xml_file_to_jsonl_file(JMDICT_XML, JMDICT_JSONL)
    puts "Converted jmdict.xml to jmdict.jsonl."
  end

  desc "Convert jmdict.xml to jmdict.jsonl (keep only priority entries)"
  task convert_priority: JMDICT_XML do
    puts "Converting jmdict.xml to jmdict.jsonl (priority entries) ..."
    Jgrind::Jmdict.convert_xml_file_to_jsonl_file_priority(
      JMDICT_XML,
      JMDICT_JSONL
    )
    puts "Converted jmdict.xml to jmdict.jsonl (priority entries)."
  end

  desc "Convert jmdict.xml to jmdict.jsonl (exclude priority entries)"
  task convert_other: JMDICT_XML do
    puts "Converting jmdict.xml to jmdict.jsonl (other entries) ..."
    Jgrind::Jmdict.convert_xml_file_to_jsonl_file_other(
      JMDICT_XML,
      JMDICT_JSONL
    )
    puts "Converted jmdict.xml to jmdict.jsonl (other entries)."
  end
end

KANJIDIC_XML = File.join(TMP_DIR, "kanjidic.xml")
file KANJIDIC_XML: TMP_DIR do
  Rake::Task["kanjidic:download"].execute
end

KANJIDIC_JSONL = File.join(DATA_DIR, "kanjidic/kanjidic.jsonl")
file KANJIDIC_JSONL do
  Rake::Task["kanjidic:convert"].execute
end

namespace :kanjidic do
  task build: %w[clean download convert]

  desc "Remove kanjidic files"
  task :clean do
    rm_rf [KANJIDIC_XML, KANJIDIC_JSONL]
  end

  desc "Download kanjidic.xml"
  task download: TMP_DIR do
    puts "Downloading kanjidic.xml ..."
    Jgrind::Kanjidic.download_to_file(KANJIDIC_XML)
    puts "Downloaded kanjidic.xml."
  end

  desc "Convert kanjidic.xml to kanjidic.jsonl"
  task convert: KANJIDIC_XML do
    puts "Converting kanjidic.xml to kanjidic.jsonl ..."
    Jgrind::Kanjidic.convert_xml_file_to_jsonl_file(
      KANJIDIC_XML,
      KANJIDIC_JSONL
    )
    puts "Converted kanjidic.xml to kanjidic.jsonl."
  end
end
