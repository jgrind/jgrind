# jgrind

Data and tools for studying the Japanese language.

## Getting Started

This project uses [git submodules]. To initialize and checkout the submodules,
run the following after cloning the repo:

```sh
git submodule init
git submodule update
```

[git submodules]: https://git-scm.com/book/en/v2/Git-Tools-Submodules
