# frozen_string_literal: true

require "json"

RSpec.describe Jgrind::Kanjidic do
  shared_context "when forwarding arguments" do
    let(:args) { %w[foo bar baz] }
    let(:kwargs) { { foo: 1, bar: 2, baz: 3 } }
    let(:block) { -> {} }
    let(:expect_block) { ->(&actual) { expect(actual).to be(block) } }
  end

  shared_context "when downloading a file" do
    let(:data) { "data" }
    let(:downloader) { double }

    before do
      allow(downloader).to receive(:download).and_return(data)
      allow(Jgrind::Kanjidic::Downloader).to receive(:new)
        .and_return(downloader)
    end
  end

  after do
    # remove cached instances
    described_class.downloader = nil
    described_class.xml_converter = nil
  end

  describe ".download_to_file" do
    include_context "when forwarding arguments"
    include_context "when downloading a file"

    let(:path) { "path/to/kanjidic.xml" }

    before { allow(File).to receive(:write) }

    def download_to_file
      described_class.download_to_file(path, *args, **kwargs, &block)
    end

    it "writes the downloaded data to a file" do
      download_to_file
      expect(File).to have_received(:write).with(path, data)
    end

    it "forwards other arguments to the downloader" do
      download_to_file
      expect(downloader).to have_received(:download)
        .with(*args, **kwargs, &expect_block)
    end
  end

  describe ".download" do
    subject(:downloaded) { described_class.download }

    include_context "when forwarding arguments"
    include_context "when downloading a file"

    it "returns the downloaded data" do
      expect(downloaded).to eq(data)
    end

    it "forwards arguments to the downloader" do
      described_class.download(*args, **kwargs, &block)
      expect(downloader).to have_received(:download)
        .with(*args, **kwargs, &expect_block)
    end
  end

  shared_context "when converting an xml file" do
    let(:converter) { double }

    before do
      allow(converter).to receive(:convert)
      allow(Jgrind::Kanjidic::XmlConverter).to receive(:new)
        .and_return(converter)
    end
  end

  describe ".convert_xml" do
    include_context "when forwarding arguments"
    include_context "when converting an xml file"

    it "forwards arguments to the xml converter" do
      described_class.convert_xml(*args, **kwargs, &block)
      expect(converter).to have_received(:convert)
        .with(*args, **kwargs, &expect_block)
    end
  end

  describe ".convert_xml_file" do
    include_context "when forwarding arguments"
    include_context "when converting an xml file"

    let(:path) { "path/to/kanjidic.xml" }
    let(:file) { double }

    before do
      allow(File).to receive(:open).with(path).and_yield(file)
    end

    it "forwards the file and arguments to the xml converter" do
      described_class.convert_xml_file(path, *args, **kwargs, &block)
      expect(converter).to have_received(:convert)
        .with(file, *args, **kwargs, &expect_block)
    end
  end

  describe ".convert_xml_file_to_jsonl_file" do
    include_context "when forwarding arguments"
    include_context "when converting an xml file"

    let(:src) { "path/to/kanjidic.xml" }
    let(:src_file) { double }
    let(:dest) { "path/to/kanjidic.jsonl" }
    let(:dest_file) { double }
    let(:records) { [{ foo: 1 }, { bar: 2 }, { baz: 3 }] }

    def receive_convert_and_yield_records
      records.each_with_object(receive(:convert)) do |record, chain|
        chain.and_yield(record)
      end
    end

    before do
      allow(converter).to receive_convert_and_yield_records
      allow(dest_file).to receive(:write)
      allow(File).to receive(:open).with(src).and_yield(src_file)
      allow(File).to receive(:open).with(dest, "w").and_yield(dest_file)
    end

    it "forwards the source file and arguments to the xml converter" do
      described_class.convert_xml_file_to_jsonl_file(src, dest, *args, **kwargs)
      expect(converter).to have_received(:convert)
        .with(src_file, *args, **kwargs)
    end

    it "writes json lines to the destination file" do
      described_class.convert_xml_file_to_jsonl_file(src, dest)
      records.each do |record|
        expect(dest_file).to have_received(:write).with(JSON.dump(record), "\n")
      end
    end
  end
end
