# frozen_string_literal: true

require "json"

require "nokogiri"

RSpec.describe Jgrind::Jmdict::XmlConverter do
  subject(:converter) { described_class.new }

  def xml(str)
    Nokogiri::XML(str).children.first
  end

  k_ele_xml = <<~XML
    <k_ele>
      <keb>keb1</keb>
      <ke_inf>info1</ke_inf>
      <ke_inf>info2</ke_inf>
      <ke_pri>news1</ke_pri>
      <ke_pri>ichi2</ke_pri>
      <ke_pri>spec1</ke_pri>
      <ke_pri>gai2</ke_pri>
      <ke_pri>nf03</ke_pri>
    </k_ele>
  XML

  k_ele = {
    keb: "keb1",
    ke_inf: %w[info1 info2],
    ke_pri: { news: 1, ichi: 2, spec: 1, gai: 2, nf: 3 }
  }

  k_ele2_xml = <<~XML
    <k_ele>
      <keb>keb2</keb>
      <ke_inf>info3</ke_inf>
      <ke_inf>info4</ke_inf>
      <ke_pri>news2</ke_pri>
      <ke_pri>ichi1</ke_pri>
      <ke_pri>spec2</ke_pri>
      <ke_pri>gai1</ke_pri>
      <ke_pri>nf04</ke_pri>
    </k_ele>
  XML

  k_ele2 = {
    keb: "keb2",
    ke_inf: %w[info3 info4],
    ke_pri: { news: 2, ichi: 1, spec: 2, gai: 1, nf: 4 }
  }

  k_ele_invalid_pri_xml = <<~XML
    <k_ele>
      <ke_pri />
      <ke_pri>invalid</ke_pri>
    </k_ele>
  XML

  describe "#convert_k_ele" do
    subject(:converted) { converter.convert_k_ele(element) }

    let(:element) { xml(k_ele_xml) }

    it "converts a <k_ele> element" do
      expect(converted).to eq(k_ele)
    end

    context "when the element is empty" do
      let(:element) { xml("<k_ele />") }

      it { is_expected.to be_nil }
    end

    context "with invalid <ke_pri> elements" do
      let(:element) { xml(k_ele_invalid_pri_xml) }

      it "ignores the <ke_pri> elements" do
        expect(converted).to be_nil
      end
    end
  end

  r_ele_xml = <<~XML
    <r_ele>
      <reb>reb1</reb>
      <re_nokanji />
      <re_restr>keb1</re_restr>
      <re_inf>info1</re_inf>
      <re_inf>info2</re_inf>
      <re_pri>news1</re_pri>
      <re_pri>ichi2</re_pri>
      <re_pri>spec1</re_pri>
      <re_pri>gai2</re_pri>
      <re_pri>nf03</re_pri>
    </r_ele>
  XML

  r_ele = {
    reb: "reb1",
    re_nokanji: true,
    re_restr: ["keb1"],
    re_inf: %w[info1 info2],
    re_pri: { news: 1, ichi: 2, spec: 1, gai: 2, nf: 3 }
  }

  r_ele2_xml = <<~XML
    <r_ele>
      <reb>reb2</reb>
      <re_restr>keb2</re_restr>
      <re_inf>info3</re_inf>
      <re_inf>info4</re_inf>
      <re_pri>news2</re_pri>
      <re_pri>ichi1</re_pri>
      <re_pri>spec2</re_pri>
      <re_pri>gai1</re_pri>
      <re_pri>nf04</re_pri>
    </r_ele>
  XML

  r_ele2 = {
    reb: "reb2",
    re_restr: ["keb2"],
    re_inf: %w[info3 info4],
    re_pri: { news: 2, ichi: 1, spec: 2, gai: 1, nf: 4 }
  }

  r_ele_invalid_pri_xml = <<~XML
    <r_ele>
      <re_pri />
      <re_pri>invalid</re_pri>
    </r_ele>
  XML

  describe "#convert_r_ele" do
    subject(:converted) { converter.convert_r_ele(element) }

    let(:element) { xml(r_ele_xml) }

    it "converts a <r_ele> element" do
      expect(converted).to eq(r_ele)
    end

    context "when the element is empty" do
      let(:element) { xml("<r_ele />") }

      it { is_expected.to be_nil }
    end

    context "with invalid <re_pri> elements" do
      let(:element) { xml(r_ele_invalid_pri_xml) }

      it "ignores the <re_pri> element" do
        expect(converted).to be_nil
      end
    end
  end

  lsource_xml = <<~XML
    <lsource xml:lang="lang" ls_type="type1" ls_wasei="waseigo">
      lsource1
    </lsource>
  XML

  lsource = {
    content: "lsource1",
    lang: "lang",
    ls_type: "type1",
    ls_wasei: "waseigo"
  }

  lsource2_xml = <<~XML
    <lsource ls_type="type2">lsource2</lsource>
  XML

  lsource2 = { content: "lsource2", ls_type: "type2" }

  lsource3_xml = <<~XML
    <lsource xml:lang="lang" ls_type="type3" ls_wasei="waseigo">
      lsource3
    </lsource>
  XML

  lsource3 = {
    content: "lsource3",
    lang: "lang",
    ls_type: "type3",
    ls_wasei: "waseigo"
  }

  lsource4_xml = <<~XML
    <lsource ls_type="type4">lsource4</lsource>
  XML

  lsource4 = { content: "lsource4", ls_type: "type4" }

  describe "#convert_lsource" do
    subject(:converted) { converter.convert_lsource(element) }

    let(:element) { xml(lsource_xml) }

    it "converts <lsource> elements" do
      expect(converted).to eq(lsource)
    end

    context "when the element is empty" do
      let(:element) { xml("<lsource />") }

      it { is_expected.to be_nil }
    end
  end

  gloss_xml = <<~XML
    <gloss g_type="type1">gloss1</gloss>
  XML

  gloss = { content: "gloss1", g_type: "type1" }

  gloss2_xml = <<~XML
    <gloss g_type="type2">gloss2</gloss>
  XML

  gloss2 = { content: "gloss2", g_type: "type2" }

  gloss3_xml = <<~XML
    <gloss g_type="type3">gloss3</gloss>
  XML

  gloss3 = { content: "gloss3", g_type: "type3" }

  gloss4_xml = <<~XML
    <gloss g_type="type4">gloss4</gloss>
  XML

  gloss4 = { content: "gloss4", g_type: "type4" }

  describe "#convert_gloss" do
    subject(:converted) { converter.convert_gloss(element) }

    let(:element) { xml(gloss_xml) }

    it "converts <gloss> elements" do
      expect(converted).to eq(gloss)
    end

    context "when the element is empty" do
      let(:element) { xml("<gloss />") }

      it { is_expected.to be_nil }
    end
  end

  sense_xml = <<~XML
    <sense>
      <stagk>keb1</stagk>
      <stagk>keb2</stagk>
      <stagr>reb1</stagr>
      <stagr>reb2</stagr>
      <pos>pos1</pos>
      <pos>pos2</pos>
      <xref>crossref1</xref>
      <xref>crossref2</xref>
      <ant>antonym1</ant>
      <ant>antonym2</ant>
      <field>field1</field>
      <field>field2</field>
      <misc>misc1</misc>
      <misc>misc2</misc>
      <s_inf>info1</s_inf>
      <s_inf>info2</s_inf>
      #{lsource_xml}
      #{lsource2_xml}
      <dial>dialect1</dial>
      <dial>dialect2</dial>
      #{gloss_xml}
      #{gloss2_xml}
    </sense>
  XML

  sense = {
    stagk: %w[keb1 keb2],
    stagr: %w[reb1 reb2],
    pos: %w[pos1 pos2],
    xref: %w[crossref1 crossref2],
    ant: %w[antonym1 antonym2],
    field: %w[field1 field2],
    misc: %w[misc1 misc2],
    s_inf: %w[info1 info2],
    lsource: [lsource, lsource2],
    gloss: [gloss, gloss2]
  }

  sense2_xml = <<~XML
    <sense>
      <stagk>keb3</stagk>
      <stagk>keb4</stagk>
      <stagr>reb3</stagr>
      <stagr>reb4</stagr>
      <pos>pos3</pos>
      <pos>pos4</pos>
      <xref>crossref3</xref>
      <xref>crossref4</xref>
      <ant>antonym3</ant>
      <ant>antonym4</ant>
      <field>field3</field>
      <field>field4</field>
      <misc>misc3</misc>
      <misc>misc4</misc>
      <s_inf>info3</s_inf>
      <s_inf>info4</s_inf>
      #{lsource3_xml}
      #{lsource4_xml}
      <dial>dialect3</dial>
      <dial>dialect4</dial>
      #{gloss3_xml}
      #{gloss4_xml}
    </sense>
  XML

  sense2 = {
    stagk: %w[keb3 keb4],
    stagr: %w[reb3 reb4],
    pos: %w[pos3 pos4],
    xref: %w[crossref3 crossref4],
    ant: %w[antonym3 antonym4],
    field: %w[field3 field4],
    misc: %w[misc3 misc4],
    s_inf: %w[info3 info4],
    lsource: [lsource3, lsource4],
    gloss: [gloss3, gloss4]
  }

  describe "#convert_sense" do
    subject(:converted) { converter.convert_sense(element) }

    let(:element) { xml(sense_xml) }

    it "converts <sense> elements" do
      expect(converted).to eq(sense)
    end

    context "when the element is empty" do
      let(:element) { xml("<sense />") }

      it { is_expected.to be_nil }
    end
  end

  entry_xml = <<~XML
    <entry>
      <ent_seq>123</ent_seq>
      #{k_ele_xml}
      #{k_ele2_xml}
      #{r_ele_xml}
      #{r_ele2_xml}
      #{sense_xml}
      #{sense2_xml}
    </entry>
  XML

  entry = {
    ent_seq: 123,
    k_ele: [k_ele, k_ele2],
    r_ele: [r_ele, r_ele2],
    sense: [sense, sense2]
  }

  describe "#convert_entry" do
    subject(:converted) { converter.convert_entry(element) }

    let(:element) { xml(entry_xml) }

    it "converts an <entry> element" do
      expect(converted).to eq(entry)
    end

    context "when the element is empty" do
      let(:element) { xml("<entry />") }

      it { is_expected.to be_nil }
    end
  end

  describe "#convert" do
    subject(:converted) do
      File.open("spec/data/jmdict.xml") do |file|
        converter.convert(file)
      end
    end

    let(:entries) do
      JSON.load_file("spec/data/jmdict.json", { symbolize_names: true })
    end

    it "loads test data" do
      expect(converted.length).to eq(4)
    end

    it "converts streams" do
      expect(converted).to eq(entries)
    end

    context "when given a block" do
      subject(:converted) do
        hashes = []
        File.open("spec/data/jmdict.xml") do |file|
          converter.convert(file) { |h| hashes << h }
        end
        hashes
      end

      it "yields each hash to the block" do
        expect(converted).to eq(entries)
      end
    end
  end
end
