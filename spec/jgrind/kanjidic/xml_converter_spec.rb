# frozen_string_literal: true

require "json"

require "nokogiri"

RSpec.describe Jgrind::Kanjidic::XmlConverter do
  let(:converter) { described_class.new }

  def xml(str)
    Nokogiri::XML(str).children.first
  end

  cp_value_xml = <<~XML
    <cp_value cp_type="type1">value1</cp_value>
  XML

  cp_value = { content: "value1", cp_type: "type1" }

  cp_value2_xml = <<~XML
    <cp_value cp_type="type2">value2</cp_value>
  XML

  cp_value2 = { content: "value2", cp_type: "type2" }

  describe "#convert_cp_value" do
    subject(:converted) { converter.convert_cp_value(element) }

    let(:element) { xml(cp_value_xml) }

    it "converts a <cp_value> element" do
      expect(converted).to eq(cp_value)
    end

    context "when the element is empty" do
      let(:element) { xml("<cp_value />") }

      it { is_expected.to be_nil }
    end
  end

  codepoint_xml = <<~XML
    <codepoint>
      #{cp_value_xml}
      #{cp_value2_xml}
    </codepoint>
  XML

  codepoint = { cp_value: [cp_value, cp_value2] }

  describe "#convert_codepoint" do
    subject(:converted) { converter.convert_codepoint(element) }

    let(:element) { xml(codepoint_xml) }

    it "converts a <codepoint> element" do
      expect(converted).to eq(codepoint)
    end

    context "when the element is empty" do
      let(:element) { xml("<codepoint />") }

      it { is_expected.to be_nil }
    end
  end

  rad_value_xml = <<~XML
    <rad_value rad_type="type1">1</rad_value>
  XML

  rad_value = { content: 1, rad_type: "type1" }

  rad_value2_xml = <<~XML
    <rad_value rad_type="type2">2</rad_value>
  XML

  rad_value2 = { content: 2, rad_type: "type2" }

  describe "#convert_rad_value" do
    subject(:converted) { converter.convert_rad_value(element) }

    let(:element) { xml(rad_value_xml) }

    it "converts a <rad_value> element" do
      expect(converted).to eq(rad_value)
    end

    context "when the element is empty" do
      let(:element) { xml("<rad_value />") }

      it { is_expected.to be_nil }
    end
  end

  radical_xml = <<~XML
    <radical>
      #{rad_value_xml}
      #{rad_value2_xml}
    </radical>
  XML

  radical = { rad_value: [rad_value, rad_value2] }

  describe "#convert_radical" do
    subject(:converted) { converter.convert_radical(element) }

    let(:element) { xml(radical_xml) }

    it "converts a <radical> element" do
      expect(converted).to eq(radical)
    end

    context "when the element is empty" do
      let(:element) { xml("<radical />") }

      it { is_expected.to be_nil }
    end
  end

  variant_xml = <<~XML
    <variant var_type="type1">variant1</variant>
  XML

  variant = { content: "variant1", var_type: "type1" }

  variant2_xml = <<~XML
    <variant var_type="type2">variant2</variant>
  XML

  variant2 = { content: "variant2", var_type: "type2" }

  describe "#convert_variant" do
    subject(:converted) { converter.convert_variant(element) }

    let(:element) { xml(variant_xml) }

    it "converts a <variant> element" do
      expect(converted).to eq(variant)
    end

    context "when the element is empty" do
      let(:element) { xml("<variant />") }

      it { is_expected.to be_nil }
    end
  end

  misc_xml = <<~XML
    <misc>
      <grade>1</grade>
      <stroke_count>2</stroke_count>
      #{variant_xml}
      #{variant2_xml}
      <freq>3</freq>
      <rad_name>name1</rad_name>
      <rad_name>name2</rad_name>
      <jlpt>4</jlpt>
    </misc>
  XML

  misc = {
    grade: 1,
    stroke_count: [2],
    variant: [variant, variant2],
    freq: 3,
    rad_name: %w[name1 name2],
    jlpt: 4
  }

  describe "#convert_misc" do
    subject(:converted) { converter.convert_misc(element) }

    let(:element) { xml(misc_xml) }

    it "converts a <misc> element" do
      expect(converted).to eq(misc)
    end

    context "when the element is empty" do
      let(:element) { xml("<misc />") }

      it { is_expected.to be_nil }
    end
  end

  dic_ref_xml = <<~XML
    <dic_ref dr_type="type1">12</dic_ref>
  XML

  dic_ref = { content: 12, dr_type: "type1" }

  dic_ref_moro_xml = <<~XML
    <dic_ref dr_type="moro" m_vol="12" m_page="34">34</dic_ref>
  XML

  dic_ref_moro = { content: 34, dr_type: "moro", m_vol: 12, m_page: 34 }

  describe "#convert_dic_ref" do
    subject(:converted) { converter.convert_dic_ref(element) }

    let(:element) { xml(dic_ref_xml) }

    it "converts a <dic_ref> element" do
      expect(converted).to eq(dic_ref)
    end

    context "when the dic_ref is a moro ref" do
      let(:element) { xml(dic_ref_moro_xml) }

      it "includes additional fields" do
        expect(converted).to eq(dic_ref_moro)
      end
    end

    context "when the element is empty" do
      let(:element) { xml("<dic_ref />") }

      it { is_expected.to be_nil }
    end
  end

  dic_number_xml = <<~XML
    <dic_number>
      #{dic_ref_xml}
      #{dic_ref_moro_xml}
    </dic_number>
  XML

  dic_number = { dic_ref: [dic_ref, dic_ref_moro] }

  describe "#convert_dic_number" do
    subject(:converted) { converter.convert_dic_number(element) }

    let(:element) { xml(dic_number_xml) }

    it "converts <dic_number> elements" do
      expect(converted).to eq(dic_number)
    end

    context "when the element is empty" do
      let(:element) { xml("<dic_number />") }

      it { is_expected.to be_nil }
    end
  end

  q_code_xml = <<~XML
    <q_code qc_type="type">code</q_code>
  XML

  q_code = { content: "code", qc_type: "type" }

  q_code_skip_misclass_xml = <<~XML
    <q_code qc_type="type" skip_misclass="stroke_count">code</q_code>
  XML

  q_code_skip_misclass = {
    content: "code",
    qc_type: "type",
    skip_misclass: "stroke_count"
  }

  describe "#convert_q_code" do
    subject(:converted) { converter.convert_q_code(element) }

    let(:element) { xml(q_code_xml) }

    it "converts a <q_code> element" do
      expect(converted).to eq(q_code)
    end

    context "when the skip_misclass attribute is present" do
      let(:element) { xml(q_code_skip_misclass_xml) }

      it "includes the attribute" do
        expect(converted).to eq(q_code_skip_misclass)
      end
    end

    context "when the element is empty" do
      let(:element) { xml("<q_code>") }

      it { is_expected.to be_nil }
    end
  end

  query_code_xml = <<~XML
    <query_code>
      #{q_code_xml}
      #{q_code_skip_misclass_xml}
    </query_code>
  XML

  query_code = { q_code: [q_code, q_code_skip_misclass] }

  describe "#convert_query_code" do
    subject(:converted) { converter.convert_query_code(element) }

    let(:element) { xml(query_code_xml) }

    it "converts a <query_code> element" do
      expect(converted).to eq(query_code)
    end

    context "when the element is empty" do
      let(:element) { xml("<query_code />") }

      it { is_expected.to be_nil }
    end
  end

  reading_xml = <<~XML
    <reading r_type="type">reading</reading>
  XML

  reading = { content: "reading", r_type: "type" }

  reading_ja_on_xml = <<~XML
    <reading r_type="ja_on" on_type="kan" r_status="jy">reading</reading>
  XML

  reading_ja_on = {
    content: "reading",
    r_type: "ja_on",
    on_type: "kan",
    r_status: "jy"
  }

  reading_ja_kun_xml = <<~XML
    <reading r_type="ja_kun" r_status="jy">reading</reading>
  XML

  reading_ja_kun = { content: "reading", r_type: "ja_kun", r_status: "jy" }

  describe "#convert_reading" do
    subject(:converted) { converter.convert_reading(element) }

    let(:element) { xml(reading_xml) }

    it "converts a <reading> element" do
      expect(converted).to eq(reading)
    end

    context "when the r_type is ja_on" do
      let(:element) { xml(reading_ja_on_xml) }

      it "includes additional attributes" do
        expect(converted).to eq(reading_ja_on)
      end
    end

    context "when the r_type is ja_kun" do
      let(:element) { xml(reading_ja_kun_xml) }

      it "includes additional attributes" do
        expect(converted).to eq(reading_ja_kun)
      end
    end

    context "when the element is empty" do
      let(:element) { xml("<reading />") }

      it { is_expected.to be_nil }
    end
  end

  meaning_xml = <<~XML
    <meaning m_lang="lang">meaning1</meaning>
  XML

  meaning = { content: "meaning1", m_lang: "lang" }

  meaning2_xml = <<~XML
    <meaning>meaning2</meaning>
  XML

  meaning2 = { content: "meaning2" }

  describe "#convert_meaning" do
    subject(:converted) { converter.convert_meaning(element) }

    let(:element) { xml(meaning_xml) }

    it "converts a <meaning> element" do
      expect(converted).to eq(meaning)
    end

    context "when the element is empty" do
      let(:element) { xml("<meaning />") }

      it { is_expected.to be_nil }
    end
  end

  rmgroup_xml = <<~XML
    <rmgroup>
      #{reading_xml}
      #{reading_ja_on_xml}
      #{reading_ja_kun_xml}
      #{meaning_xml}
      #{meaning2_xml}
    </rmgroup>
  XML

  rmgroup = {
    reading: [reading, reading_ja_on, reading_ja_kun],
    meaning: [meaning, meaning2]
  }

  rmgroup2_xml = <<~XML
    <rmgroup>
      <reading r_type="type3">reading3</reading>
      <reading r_type="type4">reading4</reading>
      <meaning>meaning3</meaning>
      <meaning m_lang="lang">meaning4</meaning>
    </rmgroup>
  XML

  rmgroup2 = {
    reading: [
      { content: "reading3", r_type: "type3" },
      { content: "reading4", r_type: "type4" }
    ],
    meaning: [
      { content: "meaning3" },
      { content: "meaning4", m_lang: "lang" }
    ]
  }

  describe "#convert_rmgroup" do
    subject(:converted) { converter.convert_rmgroup(element) }

    let(:element) { xml(rmgroup_xml) }

    it "converts a <rmgroup> element" do
      expect(converted).to eq(rmgroup)
    end

    context "when the element is empty" do
      let(:element) { xml("<rmgroup />") }

      it { is_expected.to be_nil }
    end
  end

  reading_meaning_xml = <<~XML
    <reading_meaning>
      #{rmgroup_xml}
      #{rmgroup2_xml}
    </reading_meaning>
  XML

  reading_meaning = { rmgroup: [rmgroup, rmgroup2] }

  describe "#convert_reading_meaning" do
    subject(:converted) { converter.convert_reading_meaning(element) }

    let(:element) { xml(reading_meaning_xml) }

    it "converts a <reading_meaning> element" do
      expect(converted).to eq(reading_meaning)
    end

    context "when the element is empty" do
      let(:element) { xml("<reading_meaning />") }

      it { is_expected.to be_nil }
    end
  end

  character_xml = <<~XML
    <character>
      <literal>literal</literal>
      #{codepoint_xml}
      #{radical_xml}
      #{misc_xml}
      #{dic_number_xml}
      #{query_code_xml}
      #{reading_meaning_xml}
    </character>
  XML

  character = {
    literal: "literal",
    codepoint:,
    radical:,
    misc:,
    dic_number:,
    query_code:,
    reading_meaning:
  }

  describe "#convert_character" do
    subject(:converted) { converter.convert_character(element) }

    let(:element) { xml(character_xml) }

    it "converts a <character> element" do
      expect(converted).to eq(character)
    end
  end

  describe "#convert" do
    subject(:converted) do
      File.open("spec/data/kanjidic.xml") do |file|
        converter.convert(file)
      end
    end

    let(:characters) do
      JSON.load_file("spec/data/kanjidic.json", { symbolize_names: true })
    end

    it "loads test data" do
      expect(characters.length).to eq(5)
    end

    it "converts streams" do
      expect(converted).to eq(characters)
    end

    context "when given a block" do
      subject(:converted) do
        characters = []
        File.open("spec/data/kanjidic.xml") do |file|
          converter.convert(file) { |c| characters << c }
        end
        characters
      end

      it "yields each character to the block" do
        expect(converted).to eq(characters)
      end
    end
  end
end
