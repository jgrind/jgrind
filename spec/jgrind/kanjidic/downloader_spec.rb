# frozen_string_literal: true

RSpec.describe Jgrind::Kanjidic::Downloader do
  let(:downloader) { described_class.new }
  let(:gzipped_data) { "gzipped data" }
  let(:data) { "data" }

  def stub_uri
    uri = double
    allow(uri).to receive(:open).and_yield(gzipped_data)
    allow(URI).to receive(:parse).and_return(uri)
  end

  def stub_gzip_reader
    reader = double
    allow(reader).to receive(:read).and_return(data)
    allow(Zlib::GzipReader).to receive(:new)
      .with(gzipped_data)
      .and_return(reader)
  end

  before do
    stub_uri
    stub_gzip_reader
  end

  def expect_url_opened(url)
    expect(URI).to have_received(:parse).with(url)
  end

  describe "#download" do
    subject(:downloaded) { downloader.download }

    it "downloads the file from EDRDG" do
      downloaded
      expect_url_opened("http://www.edrdg.org/kanjidic/kanjidic2.xml.gz")
    end

    it "returns the raw data" do
      expect(downloaded).to eq(data)
    end

    context "when provided a URL" do
      url = "https://example.com/kanjidic.xml.gz"

      subject(:downloaded) { downloader.download(url) }

      it "downloads from the provided URL" do
        downloader.download(url)
        expect_url_opened(url)
      end

      it "returns the raw data" do
        expect(downloaded).to eq(data)
      end
    end
  end
end
