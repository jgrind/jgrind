# frozen_string_literal: true

require_relative "jgrind/jmdict"
require_relative "jgrind/kanjidic"

# Data and tools for studying the Japanese language.
module Jgrind
end
