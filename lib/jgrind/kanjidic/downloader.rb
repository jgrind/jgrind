# frozen_string_literal: true

require "open-uri"
require "zlib"

module Jgrind
  module Kanjidic
    # Downloads a KANJIDIC2 XML file.
    class Downloader
      KANJIDIC2_URL = "http://www.edrdg.org/kanjidic/kanjidic2.xml.gz"

      def download(url = nil)
        url ||= self.class::KANJIDIC2_URL
        URI.parse(url).open do |resource|
          Zlib::GzipReader.new(resource).read
        end
      end
    end
  end
end
