# frozen_string_literal: true

require "nokogiri"

# rubocop:disable Metrics/ClassLength
module Jgrind
  module Kanjidic
    # Converts a KANJIDIC2 XML file to an array of hashes. The structure of each
    # hash corresponds to the original KANJIDIC2 XML schema.
    #
    # For more information:
    # http://www.edrdg.org/wiki/index.php/KANJIDIC_Project#Content_&_Format
    class XmlConverter
      def convert(stream)
        doc = Nokogiri::XML(stream)
        elements = doc.search("character")

        if block_given?
          elements.each { |e| yield convert_character(e) }
        else
          elements.map { |e| convert_character(e) }
        end
      end

      def convert_character(element)
        compact({
          literal: search_content(element, "literal")&.first,
          codepoint: convert_codepoint(search_first(element, "codepoint")),
          radical: convert_radical(search_first(element, "radical")),
          misc: convert_misc(search_first(element, "misc")),
          dic_number: convert_dic_number(search_first(element, "dic_number")),
          query_code: convert_query_code(search_first(element, "query_code")),
          reading_meaning: convert_reading_meaning(
            search_first(element, "reading_meaning")
          )
        })
      end

      def convert_codepoint(element)
        compact({
          cp_value: search_map(element, "cp_value") { |e| convert_cp_value(e) }
        })
      end

      def convert_cp_value(element)
        compact({
          content: compact_content(element),
          cp_type: element.attr("cp_type")
        })
      end

      def convert_radical(element)
        compact({
          rad_value: search_map(element, "rad_value") do |e|
            convert_rad_value(e)
          end
        })
      end

      def convert_rad_value(element)
        compact({
          content: compact_content(element)&.to_i,
          rad_type: element.attr("rad_type")
        })
      end

      def convert_misc(element)
        compact({
          grade: search_first_content(element, "grade")&.to_i,
          stroke_count: search_content(element, "stroke_count")&.map(&:to_i),
          variant: search_map(element, "variant") { |e| convert_variant(e) },
          freq: search_first_content(element, "freq")&.to_i,
          rad_name: search_content(element, "rad_name"),
          jlpt: search_first_content(element, "jlpt")&.to_i
        })
      end

      def convert_variant(element)
        compact({
          content: compact_content(element),
          var_type: element.attr("var_type")
        })
      end

      def convert_dic_number(element)
        compact({
          dic_ref: search_map(element, "dic_ref") { |e| convert_dic_ref(e) }
        })
      end

      def convert_dic_ref(element)
        compact({
          content: compact_content(element)&.to_i,
          dr_type: element.attr("dr_type"),
          m_vol: element.attr("m_vol")&.to_i,
          m_page: element.attr("m_page")&.to_i
        })
      end

      def convert_query_code(element)
        compact({
          q_code: search_map(element, "q_code") { |e| convert_q_code(e) }
        })
      end

      def convert_q_code(element)
        compact({
          content: compact_content(element),
          qc_type: element.attr("qc_type"),
          skip_misclass: element.attr("skip_misclass")
        })
      end

      def convert_reading_meaning(element)
        compact({
          rmgroup: search_map(element, "rmgroup") { |e| convert_rmgroup(e) },
          nanori: search_content(element, "nanori")
        })
      end

      def convert_rmgroup(element)
        compact({
          reading: search_map(element, "reading") { |e| convert_reading(e) },
          meaning: search_map(element, "meaning") { |e| convert_meaning(e) }
        })
      end

      def convert_reading(element)
        compact({
          content: compact_content(element),
          r_type: element.attr("r_type"),
          on_type: element.attr("on_type"),
          r_status: element.attr("r_status")
        })
      end

      def convert_meaning(element)
        compact({
          content: compact_content(element),
          m_lang: element.attr("m_lang")
        })
      end

      private

      def search_first_content(element, search)
        compact_content(search_first(element, search))
      end

      def search_first(element, search)
        element&.search(search)&.first
      end

      def search_content(element, search)
        compact(search_map(element, search) { |e| compact_content(e) })
      end

      def search_map(element, search, &)
        compact(element&.search(search)&.map(&))
      end

      def compact_content(element)
        compact(element&.inner_html&.strip)
      end

      def compact(value)
        value = value.compact if value.respond_to?(:compact)
        return nil if value.nil? || value.empty?

        value
      end
    end
  end
end
# rubocop:enable Metrics/ClassLength
