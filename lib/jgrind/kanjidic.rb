# frozen_string_literal: true

require "json"

require_relative "kanjidic/downloader"
require_relative "kanjidic/xml_converter"

module Jgrind
  # Utilities for working with Kanjidic files
  module Kanjidic
    class << self
      attr_writer :downloader, :xml_converter

      def download_to_file(path, ...)
        File.write(path, download(...))
      end

      def download(...)
        downloader.download(...)
      end

      def convert_xml_file_to_jsonl_file(src, dest, *args, **kwargs)
        File.open(dest, "w") do |file|
          convert_xml_file(src, *args, **kwargs) do |character|
            file.write(JSON.dump(character), "\n")
          end
        end
      end

      def convert_xml_file(path, ...)
        File.open(path) do |file|
          convert_xml(file, ...)
        end
      end

      def convert_xml(...)
        xml_converter.convert(...)
      end

      def downloader
        @downloader ||= Jgrind::Kanjidic::Downloader.new
      end

      def xml_converter
        @xml_converter ||= Jgrind::Kanjidic::XmlConverter.new
      end
    end
  end
end
