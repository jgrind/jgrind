# frozen_string_literal: true

require_relative "jmdict/downloader"
require_relative "jmdict/xml_converter"

module Jgrind
  # Utilities for working with JMdict files
  module Jmdict
    class << self
      attr_writer :downloader, :xml_converter

      def download_to_file(path, ...)
        File.write(path, download(...))
      end

      def download(...)
        downloader.download(...)
      end

      def download_e_to_file(path)
        File.write(path, download_e)
      end

      def download_e
        downloader.download_e
      end

      def convert_xml_file_to_jsonl_file_priority(src, dest, *args, **kwargs)
        convert_xml_file_to_jsonl_file(src, dest, *args, **kwargs) do |record|
          priority?(record)
        end
      end

      def convert_xml_file_to_jsonl_file_other(src, dest, *args, **kwargs)
        convert_xml_file_to_jsonl_file(src, dest, *args, **kwargs) do |record|
          !priority?(record)
        end
      end

      def convert_xml_file_to_jsonl_file(src, dest, *args, **kwargs)
        File.open(dest, "w") do |file|
          convert_xml_file(src, *args, **kwargs) do |record|
            next if block_given? && !(yield record)

            file.write(JSON.dump(record), "\n")
          end
        end
      end

      def convert_xml_file(path, ...)
        File.open(path) do |file|
          convert_xml(file, ...)
        end
      end

      def convert_xml(...)
        xml_converter.convert(...)
      end

      def priority?(entry)
        ke_pri?(entry) || re_pri?(entry)
      end

      def ke_pri?(entry)
        return false if entry.nil? || entry[:k_ele].nil?

        !entry[:k_ele].all? { |e| e[:ke_pri].nil? || e[:ke_pri].empty? }
      end

      def re_pri?(entry)
        return false if entry.nil? || entry[:r_ele].nil?

        !entry[:r_ele].all? { |e| e[:re_pri].nil? || e[:re_pri].empty? }
      end

      private

      def downloader
        @downloader ||= Jmdict::Downloader.new
      end

      def xml_converter
        @xml_converter ||= Jmdict::XmlConverter.new
      end
    end
  end
end
