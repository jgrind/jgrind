# frozen_string_literal: true

require "open-uri"
require "zlib"

module Jgrind
  module Jmdict
    # Downloads a JMdict XML file.
    class Downloader
      JMDICT_URL = "http://ftp.edrdg.org/pub/Nihongo/JMdict.gz"
      JMDICT_E_URL = "http://ftp.edrdg.org/pub/Nihongo/JMdict_e.gz"

      def download_e
        download(self.class::JMDICT_E_URL)
      end

      def download(url = nil)
        url ||= self.class::JMDICT_URL
        URI.parse(url).open do |resource|
          Zlib::GzipReader.new(resource).read
        end
      end
    end
  end
end
