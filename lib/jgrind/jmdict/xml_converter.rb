# frozen_string_literal: true

require "nokogiri"

module Jgrind
  module Jmdict
    # Converts a JMdict XML file to an array of hashes. The structure of each
    # hash corresponds to the original JMdict XML schema.
    #
    # For more information:
    # https://www.edrdg.org/wiki/index.php/JMdict-EDICT_Dictionary_Project#FORMAT
    class XmlConverter
      def convert(stream)
        doc = Nokogiri::XML(stream)
        entries = doc.search("entry")

        if block_given?
          entries.each { |e| yield convert_entry(e) }
        else
          entries.map { |e| convert_entry(e) }
        end
      end

      def convert_entry(element)
        compact({
          ent_seq: search_first_content(element, "ent_seq")&.to_i,
          k_ele: search_map(element, "k_ele") { |e| convert_k_ele(e) },
          r_ele: search_map(element, "r_ele") { |e| convert_r_ele(e) },
          sense: search_map(element, "sense") { |e| convert_sense(e) }
        })
      end

      def convert_k_ele(element)
        compact({
          keb: search_first_content(element, "keb"),
          ke_inf: search_content(element, "ke_inf"),
          ke_pri: convert_pri(element, "ke_pri")
        })
      end

      def convert_r_ele(element)
        compact({
          reb: search_first_content(element, "reb"),
          re_nokanji: element.search("re_nokanji")&.any? || nil,
          re_restr: search_content(element, "re_restr"),
          re_inf: search_content(element, "re_inf"),
          re_pri: convert_pri(element, "re_pri")
        })
      end

      def convert_sense(element)
        compact({
          stagk: search_content(element, "stagk"),
          stagr: search_content(element, "stagr"),
          pos: search_content(element, "pos"),
          xref: search_content(element, "xref"),
          ant: search_content(element, "ant"),
          field: search_content(element, "field"),
          misc: search_content(element, "misc"),
          s_inf: search_content(element, "s_inf"),
          lsource: search_map(element, "lsource") { |e| convert_lsource(e) },
          gloss: search_map(element, "gloss") { |e| convert_gloss(e) }
        })
      end

      def convert_lsource(element)
        compact({
          content: compact_content(element),
          lang: element.attr("xml:lang"),
          ls_type: element.attr("ls_type"),
          ls_wasei: element.attr("ls_wasei")
        })
      end

      def convert_gloss(element)
        compact({
          content: compact_content(element),
          g_type: element.attr("g_type")
        })
      end

      private

      def convert_pri(element, search)
        compact(
          search_map(element, search) { |e| pri_key_value(compact_content(e)) }
            &.select { |(k, v)| k && v }
            &.each_with_object({}) { |(k, v), p| p.update({ k => v }) }
        )
      end

      def pri_key_value(content)
        captures = content&.match(/([a-z]+)(\d+)/)&.captures
        return [nil, nil] unless captures && captures.count == 2

        [captures[0].to_sym, captures[1].to_i]
      end

      def search_first_content(element, search)
        compact_content(search_first(element, search))
      end

      def search_first(element, search)
        element&.search(search)&.first
      end

      def search_content(element, search)
        compact(search_map(element, search) { |e| compact_content(e) })
      end

      def search_map(element, search, &)
        compact(element&.search(search)&.map(&))
      end

      def compact_content(element)
        compact(element&.inner_html&.strip)
      end

      def compact(value)
        value = value.compact if value.respond_to?(:compact)
        return nil if value.nil? || value.empty?

        value
      end
    end
  end
end
