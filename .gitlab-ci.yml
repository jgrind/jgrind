workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: always

stages:
  - lint
  - test
  - update_data
  - update

.ruby:
  image: "ruby:3.1"
  before_script: &ruby_before_script
    - ruby --version
    - gem install bundler -v '~> 2.4'
    - bin/bundle install

.scheduled:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_PIPELINE_SOURCE == "manual"'

.not_scheduled:
  rules:
    - if: '$CI_PIPELINE_SOURCE != "schedule"'

rubocop:
  extends:
    - .ruby
    - .not_scheduled
  stage: lint
  script:
    - bin/rubocop

rspec:
  extends:
    - .ruby
    - .not_scheduled
  stage: test
  script:
    - bin/rspec

.setup_git: &setup_git
  - apt-get install --yes git
  - git config --global user.name "${GITLAB_USER_NAME}"
  - git config --global user.email "${GITLAB_USER_EMAIL}"
  - git remote set-url origin git@${CI_SERVER_HOST}:${CI_PROJECT_PATH}

.setup_ssh: &setup_ssh
  - apt-get install --yes openssh-client
  - eval $(ssh-agent -s)
  - mkdir -p ~/.ssh
  - chmod 700 ~/.ssh
  - ssh-keyscan "${CI_SERVER_HOST}" >> ~/.ssh/known_hosts
  - chmod 644 ~/.ssh/known_hosts

# A deploy key pair is required in order for the pipeline to authenticate with
# GitLab and push new commits to a repo.
#
# To add a deploy key pair to GitLab:
#
# 1. Generate a key pair:
#
#   $ ssh-keygen -t ed25519 -a 100
#
# 2. Base64 encode the private key:
#
#   $ cat path/to/private_key | base64
#
# 3. Add the Base64-encoded private key as a CI environment variable.
#
#   Type: Variable
#   Key: DEPLOY_PRIVATE_KEY
#   Value: (the Base64-encoded private key)
#   Enabled Flags:
#     - Protect variable
#     - Mask variable
#
#   Note that this variable will only be permitted on protected branches.
#
# 4. Add the public key as a deploy key to the repo(s) that will be pushed to.
#
.setup_deploy_keys: &setup_deploy_keys
  - echo "${DEPLOY_PRIVATE_KEY}" | base64 --decode | tr -d '\r' | ssh-add -

.check_for_changes: &check_for_changes
  - git status --untracked-files
  - |
    if [[ ! -n "$(git status --porcelain)" ]]; then
      echo "No changes found!"
      exit 0
    fi

.set_version: &set_version
  - VERSION=$(date --utc +%Y-%m-%d)
  - |
    if [[ "$CI_COMMIT_REF_NAME" != "$CI_DEFAULT_BRANCH" ]]; then
      VERSION="$VERSION-$CI_COMMIT_REF_NAME"
    fi
  - echo "$VERSION" > VERSION
  - cat VERSION

.commit_data: &commit_data
  - git add --all .
  - git status --untracked-files
  - git commit --message "Update data ($VERSION)"

.push_data: &push_data
  - *check_for_changes
  - *set_version
  - *commit_data
  - git push --push-option ci.skip origin "$CI_COMMIT_REF_NAME"

.push_tag: &push_tag
  - git tag "$VERSION"
  - git push --push-option ci.skip origin "$VERSION"

.update:
  extends:
    - .ruby
    - .scheduled
  stage: update
  before_script:
    - *ruby_before_script
    - apt-get update --yes
    - *setup_git
    - *setup_ssh
    - *setup_deploy_keys
    - git submodule init

.update_data:
  extends:
    - .update
  stage: update_data

update_jmdict:
  extends:
    - .update_data
  script:
    - git submodule update data/jmdict
    - cd data/jmdict
    - git checkout "$CI_COMMIT_REF_NAME"
    - (cd ../.. && bin/rake jmdict:build)
    - *push_data
    - *push_tag

update_kanjidic:
  extends:
    - .update_data
  script:
    - git submodule update data/kanjidic
    - cd data/kanjidic
    - git checkout "$CI_COMMIT_REF_NAME"
    - (cd ../.. && bin/rake kanjidic:build)
    - *push_data
    - *push_tag

update:
  extends:
    - .update
  script:
    - git checkout "$CI_COMMIT_REF_NAME"
    - git submodule update --remote
    - cd data
    - *push_data
    - VERSION="data-$VERSION"
    - *push_tag
